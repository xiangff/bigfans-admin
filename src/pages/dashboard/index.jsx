import React from 'react';
import { Card,  Breadcrumb,Badge ,Row,Col,Icon} from 'antd';

class DashboardPage extends React.Component {

    render() {
        return (
            <div id="dashboardPage" className="dashboardPage">
                <Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                </Breadcrumb>
                <Row gutter={16}>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>今日订单总数</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>今日会员总数</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>今日访问量</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>xxxxxxx</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                </Row>
                <Row gutter={16} style={{'margin-top':'25px'}}>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>待处理订单</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>商品数量</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>会员总数</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <a style={{display:'block' , textAlign:'center'}}>
                            <Icon type="desktop" style={{width:'60px' , height:'60px', fontSize: 60}}/>
                            <br/>
                            <span style={{fontSize:'16px'}}>xxxxxxx</span>
                            <br/>
                            <span style={{fontSize:'16px'}}>198</span>
                            </a>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default DashboardPage;